/******************************************************************************
 * ctcp.c
 * ------
 * Implementation of cTCP done here. This is the only file you need to change.
 * Look at the following files for references and useful functions:
 *   - ctcp.h: Headers for this file.
 *   - ctcp_iinked_list.h: Linked list functions for managing a linked list.
 *   - ctcp_sys.h: Connection-related structs and functions, cTCP segment
 *                 definition.
 *   - ctcp_utils.h: Checksum computation, getting the current time.
 *
 *****************************************************************************/

#include <errno.h>
#include "ctcp.h"
#include "ctcp_linked_list.h"
#include "ctcp_sys.h"
#include "ctcp_utils.h"


#define CTCP_HEADER_LEN  sizeof(ctcp_segment_t) 
/**
 * Connection state.
 *
 * Stores per-connection information such as the current sequence number,
 * unacknowledged packets, etc.
 *
 * You should add to this to store other fields you might need.
 */
struct ctcp_state {
  struct ctcp_state *next;  /* Next in linked list */
  struct ctcp_state **prev; /* Prev in linked list */

  conn_t *conn;             /* Connection object -- needed in order to figure
                               out destination when sending */
  linked_list_t *segments;  /* Linked list of segments sent to this connection.
                               It may be useful to have multiple linked lists
                               for unacknowledged segments, segments that
                               haven't been sent, etc. Lab 1 uses the
                               stop-and-wait protocol and therefore does not
                               necessarily need a linked list. You may remove
                               this if this is the case for you */

  /* FIXME: Add other needed fields. */
  int seqno; /* Current sequence number of the connection */
  int ackno; /* Current acknowledge number of the connection */
  int retransmissions;
  char fin_sent;
  char fin_acked;
  char fin_received;
  ctcp_segment_t *last_received_segment;
  ctcp_segment_t *last_sent_segment;
  long last_retransmission_time;
  char lss_acked; 
  ctcp_config_t *cfg;

};

/**
 * Linked list of connection states. Go through this in ctcp_timer() to
 * resubmit segments and tear down connections.
 */
static ctcp_state_t *state_list;

/* FIXME: Feel free to add as many helper functions as needed. Don't repeat
          code! Helper functions make the code clearer and cleaner. */



/* Reports error and exits current method */

void error(char *msg) {
  fprintf(stderr, "%s: %s\n", msg, strerror(errno));
}

void error_exit(char *msg) {
  error(msg);
  exit(1);
}


ctcp_state_t *ctcp_init(conn_t *conn, ctcp_config_t *cfg) {
  /* Connection could not be established. */
  if (conn == NULL) {
    return NULL;
  }

  /* Established a connection. Create a new state and update the linked list
     of connection states. */
  ctcp_state_t *state = calloc(sizeof(ctcp_state_t), 1);
  state->next = state_list;
  state->prev = &state_list;
  if (state_list)
    state_list->prev = &state->next;
  state_list = state;

  /* Set fields. */
  state->cfg = cfg;
  state->conn = conn;
  state->seqno = 1;
  state->ackno = 1;
  state->retransmissions = 0;
  state->last_received_segment = NULL;
  state->last_sent_segment = NULL;
  state->last_retransmission_time  = 0;

  /* Initially  no last sent segments to be acked */
  state->lss_acked = 1;
 
  return state;
}

void ctcp_destroy(ctcp_state_t *state) {
  printf("DEBUG: DESTROY \n");
  /* Update linked list. */
  if (state->next)
    state->next->prev = state->prev;

  *state->prev = state->next;
  conn_remove(state->conn);


  /* FIXME: Do any other cleanup here. */
  if (state->last_received_segment != NULL) 
    free(state->last_received_segment);

  if (state->last_sent_segment != NULL)
    free(state->last_sent_segment);
    
  free(state);
  end_client();
}




void prepare_new_segment(ctcp_state_t *state,int read, char *buf, uint32_t flags, ctcp_segment_t *new_segment) {
    uint16_t len = CTCP_HEADER_LEN + read;

    /* set seqno in network order*/
    new_segment->seqno = htonl(state->seqno); 
    /* set ackno in network order */
    new_segment->ackno = htonl(state->ackno); 
    /* len is sizeof(ctcp_segment_t) + len of data in network order */
    new_segment->len = htons(len); 
    /* Set ack flag to piggyback ack with data */
    new_segment->flags = flags; 
    /* Set window size in network order */
    new_segment->window = htons(MAX_SEG_DATA_SIZE);
    /* Initially set cksum to 0 for later checksum evaluation */
    new_segment->cksum = 0;
    /* Copy data from buffer to end of segment where data resides */
    memcpy(new_segment->data, buf, read);

    /* Compute and set checkum */
    uint16_t checksum = cksum(new_segment, len);
    new_segment->cksum = checksum;
}



void ctcp_read(ctcp_state_t *state) {

    /* If last sent segment has not been acked yet, don't send new data */
    if (!state->lss_acked || state->fin_sent) 
        return;


    char buf[MAX_SEG_DATA_SIZE];
    memset(buf, 0, MAX_SEG_DATA_SIZE);

    int read = conn_input(state->conn, &buf, MAX_SEG_DATA_SIZE); 
    ctcp_segment_t *new_segment;
    uint32_t flags = 0;

    if (read == 0) 
        return;
    else if (read < 0) {
        read = 0;
        flags |= htonl(FIN);
        state->fin_sent = 1;
    }
    
    int len = CTCP_HEADER_LEN + read;
    new_segment = malloc(len);
    if (new_segment == NULL) {
        error("Memory Error");
        return;
    }

    
    prepare_new_segment(state, read, buf, flags, new_segment);
    conn_send(state->conn, new_segment, len);

    state->last_sent_segment = new_segment;
    state->lss_acked = 0;
    state->retransmissions = 0;
    state->last_retransmission_time = current_time();
}


int data_len(ctcp_segment_t *segment) {
    return ntohs(segment->len) - CTCP_HEADER_LEN;
}


void ack_segment(ctcp_state_t *state) {
    ctcp_segment_t *new_segment = malloc(CTCP_HEADER_LEN);
    if (new_segment == NULL) {
        error("Memory Error");
        return;
    }
    
    uint32_t flags = 0 | htonl(ACK);
    prepare_new_segment(state, 0, NULL, flags, new_segment);
    conn_send(state->conn, new_segment, CTCP_HEADER_LEN);
    free(new_segment);
}

int corrupted(ctcp_segment_t *segment) {
    uint16_t checksum = segment->cksum;
    uint16_t len = ntohs(segment->len);
    segment->cksum = 0;
    uint16_t local_checksum = cksum(segment, len);
    segment->cksum = checksum;
    /* Returns true if segment cksum is not equal to calculated cksum */
    return checksum != local_checksum ;
}


char has_flag(ctcp_segment_t *segment, long flag) {
    return (segment->flags & htonl(flag)) != 0;
}

void ctcp_receive(ctcp_state_t *state, ctcp_segment_t *segment, size_t len) {


    if (corrupted(segment)) {
        return;
    }   

    if (state->last_received_segment != NULL) {
        ctcp_segment_t *last_received_segment = state->last_received_segment;

        /* Received duplicate segment */
        if (last_received_segment->cksum == segment->cksum) {

            /* If duplicate segment has data, send ACK and return */
            /* If duplicate segment is FIN send ACK and return */
            if (data_len(segment) > 0 || has_flag(segment, FIN))
                ack_segment(state);
            return;
        }
        /*  Received new segment */
        else  {
            free(state->last_received_segment);
            state->last_received_segment = NULL;
        }
    }

    if (has_flag(segment, ACK)) {
        /* ACK segment has been received */
        int ackno = ntohl(segment->ackno);
        state->seqno = ackno;

        /* Now can send more data, since last sent segment is ACKed */
        if (state->last_sent_segment != NULL) {
            free(state->last_sent_segment);
            state->last_sent_segment = NULL;
            state->lss_acked = 1;
        }


        // Need to close session if FIN has been sent before 
        // and FIN has been received from other side 
        // Since we already checked for ACK flag, this means
        // our FIN has been ACKed. Can close the session.
        if (state->fin_sent && state->fin_received && state->lss_acked == 1)
            ctcp_destroy(state);
    }

    state->last_received_segment = segment;

    if (has_flag(segment, FIN)) {
        /* FIN segment has been received, print EOF */
        state->ackno += 1;
        ack_segment(state);
        conn_output(state->conn, NULL, 0);
        state->fin_received = 1;
        // No need to call ctcp_output anymore, since ACK has been sent 
        // and conn_output printed EOF in stderr
        return;
    }

    /* Outputs and ACKs segments with data */
    ctcp_output(state);
}



void ctcp_output(ctcp_state_t *state) {
    size_t bufspace = conn_bufspace(state->conn);
    if (bufspace == 0) return;

    ctcp_segment_t *segment  = state->last_received_segment;
    int len = data_len(segment);

    if (len == 0) {
        return;
    }

    conn_output(state->conn, segment->data, len);
    state->ackno += len;
    ack_segment(state);
}

void ctcp_timer() {
  if (state_list == NULL) 
    return;
    
  ctcp_state_t *current_state = state_list;
  ctcp_config_t *cfg;

  while(current_state != NULL) {
    if (current_state->lss_acked == 0) {
        cfg = current_state->cfg;

        if (current_state->retransmissions == 5)
            ctcp_destroy(current_state);
        /* 
         * if last retransmission time has been more then cfg->rt_timeout, 
         * retransmit again. Skip current state otherwise.
         */

        if ((current_time() - current_state->last_retransmission_time)
                                         < cfg->rt_timeout)
            continue;

        ctcp_segment_t *new_segment = current_state->last_sent_segment;
        int len = ntohs(new_segment->len);
        conn_send(current_state->conn, new_segment, len);
        current_state->last_retransmission_time = current_time();
        current_state->retransmissions += 1;
    }
    current_state = current_state->next;
  }
}
